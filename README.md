# How to make this page

## An introduction to using the StackEdit Markdown editor in order to create a simple webpage.
English | [Deutsch](#)
#### StackEdit Account

> This step is not necessary but it allows you to directly synchronize your StackEdit work with GitHub, GitLab, CouchDB, etc.

Go to [StackEdit](https://stackedit.io/app) and create an account by clicking on the icon in the upper right corner and signing in with Google.

![StackEdit Account](https://scampsblog.com/assets/video-for-how-to-make-this-page/StackEdit-open-it.gif)

#### Your Markdown
Now start writing your Markdown using StackEdit.

![Writing](https://scampsblog.com/assets/video-for-how-to-make-this-page/StackEdit-write.gif)

You can use the Markdown Cheat Sheet to take a look at the possibilities StackEdit's Markdown offers.

![Cheat Sheet](https://scampsblog.com/assets/video-for-how-to-make-this-page/StackEdit-cheat-sheet.gif)

#### Exporting your code

Export your work as HTML, PDF, Pandoc or Markdown

> PDF and Pandoc require you to workmake a donation.

> You can also publish it to a cloud service or synchronize it with GitLab, GitHub, etc.

![Exporting](https://scampsblog.com/assets/video-for-how-to-make-this-page/StackEdit-export.gif)

Clicking OK will create a complete HTML File (if you chose Styled HTMLl, the HTML will contain a link to a css file).
Clicking COPY will, well, copy the code. The same applies as above if you chose Styled HTML.

#### Congratz! You've just created this webpage. Okay, at least its content. 
> I've added the following CSS in order to center the content. This CSS is located in a main.css file.:

``` css
.container {
	display: flex;
	justify-content: center;
	align-items: center;
}
```

Which means that my boilerplate code looks something like this:

``` html
<!DOCTYPE  html>
<html  lang="en">
	<head>
		<meta  charset="UTF-8">
		<meta  name="viewport"  content="width=device-width, initial-scale=1.0">
		<meta  http-equiv="X-UA-Compatible"  content="ie=edge">
		<title>How to make this page</title>
		<link  rel="stylesheet"  href="/css/main.css">
		<link  rel="stylesheet"  href="https://stackedit.io/style.css"  />
	</head>
	<body>
		<div  class="container">
			<div  class="inner-container">
				<div class="stackedit__html">
					<h1  id="how-to-make-this-page">How to make this page</h1>
					<!-- other content -->
				</div>
			</div>
		</div>
	</body>
</html>
```
> You can download the full source code on [GitLab](https://gitlab.com/jakoblbk/how-to-make-this-page/tree/master)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE2MTI1NDY0NDcsLTE5Mjg1MTcwMThdfQ
==
-->